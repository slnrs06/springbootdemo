package com.bravo.demo.models;


import javax.persistence.*;

@Entity
@Table(name = "books")
public class Book {
    //primary key  -- every database has a primary key

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    // Map properties fields
    @Column(nullable = false, length = 100)
    private String title;

    @Column(nullable = false)
    private String author;


    // CONSTRUCTORS
    public Book(Integer id, String title, String author) {
        this.id = id;
        this.title = title;
        this.author = author;
    }


    public Book() {
    }


    //Getters/Setters

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }








}
