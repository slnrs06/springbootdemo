package com.bravo.demo.Repositories;


import com.bravo.demo.models.Employee;
import com.bravo.demo.models.Grocery;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroceryRepository extends JpaRepository<Grocery, Long> {
}


