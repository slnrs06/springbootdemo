package com.bravo.demo.Repositories;


import com.bravo.demo.models.Ad;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdRepo extends JpaRepository<Ad, Long> {

    Ad findByTitle(String title);








}
