package com.bravo.demo.Repositories;

import com.bravo.demo.models.resaleItems;
import org.springframework.data.jpa.repository.JpaRepository;

public interface resaleItemRepo extends JpaRepository<resaleItems, Long> {

    resaleItems findByTitle(String title);

}
