package com.bravo.demo.Services;


import com.bravo.demo.Repositories.GroceryRepository;
import com.bravo.demo.models.Employee;
import com.bravo.demo.models.Grocery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class GroceryServiceImpl implements GroceryService{


    @Autowired
    private GroceryRepository groceryRepository;



    @Override
    public List<Grocery> getAllGroceries() {
        return groceryRepository.findAll();
    }




    @Override
    public void saveGrocery(Grocery grocery) {
        this.groceryRepository.save(grocery);
    }



    @Override
    public Grocery getGroceryById(long id) {
        Optional<Grocery> optional = groceryRepository.findById(id);
        Grocery grocery = null;
        if (optional.isPresent()){
            grocery = optional.get();
        } else {
            throw new RuntimeException("Grocery not found for id:" + id);

        }
        return grocery;
    }

    @Override
    public void deleteGroceryById(long id) {

        this.groceryRepository.deleteById(id);

    }


    @Override
    public Page<Grocery> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection) {
        Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name())?Sort.by(sortField).ascending():Sort.by(sortField).descending();

        Pageable pageable = PageRequest.of(pageNo-1, pageSize, sort);
        return this.groceryRepository.findAll(pageable);
    }



}
