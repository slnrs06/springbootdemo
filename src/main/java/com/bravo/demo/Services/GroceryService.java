package com.bravo.demo.Services;

import com.bravo.demo.models.Grocery;
import org.springframework.data.domain.Page;

import java.util.List;

public interface GroceryService {
    List<Grocery> getAllGroceries();

    void saveGrocery(Grocery grocery);

    Grocery getGroceryById(long id);

    void deleteGroceryById(long id);
    Page<Grocery> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection);
}
