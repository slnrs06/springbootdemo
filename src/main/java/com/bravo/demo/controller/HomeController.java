package com.bravo.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

//@Controller
public class HomeController {
//
//    @GetMapping("/")
//    @ResponseBody // this will configure the method as the response
//    public String greeting(){
//        return "Welcome to my landing page";
//    }
//
//
//    @GetMapping("/home") // in this example we dont have the @ResponseBody because HTML (HOME) WILL BE RETURNED.
//    public String welcome(){
//
//        return "home";
//    }
//
//
//    @GetMapping("/hello/{name}")
//    public String sayHello(@PathVariable String name, Model model){
//        // model is a class from the builtin spring.ui within spring framework NOT part of the MVC design patten
//
//        model.addAttribute("name", name);
//
//        return "hello";
//
//
//    }

}
