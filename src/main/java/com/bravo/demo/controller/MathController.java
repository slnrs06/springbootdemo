package com.bravo.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class MathController {
    @GetMapping(path = "/add/{num1}/and/{num2}")
    @ResponseBody
    public int addNumbers(@PathVariable int num1, @PathVariable int num2){
        return num1 + num2;
    }


    //subtract method
    @GetMapping(path = "/subtract/{num1}/and/{num2}")
    @ResponseBody
    public int subtractNumbers(@PathVariable int num1, @PathVariable int num2){
        return num1 - num2;
    }

    @GetMapping(path = "/multiply/{num1}/and/{num2}")
    @ResponseBody
    public int multiplyNumbers(@PathVariable int num1, @PathVariable int num2){
        return num1 * num2;
    }
    @GetMapping(path = "/divide/{num1}/and/{num2}")
    @ResponseBody
    public int divideNumbers(@PathVariable int num1, @PathVariable int num2){
        return num1 / num2;
    }
}
