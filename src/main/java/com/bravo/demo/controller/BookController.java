package com.bravo.demo.controller;


import com.bravo.demo.Repositories.BookRepository;
import com.bravo.demo.models.Book;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

public class BookController {

    // repository instance/variable
    // Dependency Injection
    private final BookRepository bookRepository;

    //Dependency Injection
    public BookController(BookRepository bookRepository){
        this.bookRepository = bookRepository;
    }

    @GetMapping("/books") //url
    public String index(Model model){
        model.addAttribute("books", bookRepository.findAll());
        // return this book/index
        return "books/index";
    }


    //GetMapping for when we hit submit on our form
    @GetMapping("books/form")
    public String showAdBookForm(Model model){
        model.addAttribute("book", new Book());
        return "books/add-book"; // add-book  = name of our view
    }



    // add a book

    // Post Mapping to our /books that shows our book list
    @PostMapping("books/form")
    public String addBook(@ModelAttribute Book book){
        // save a book entry
        bookRepository.save(book);
        return "redirect:/books";
    }

    // Edit part 1
    @GetMapping("books/{id}/edit-book")
    public String viewEditBook(@PathVariable Integer id, Model model){
        model.addAttribute("book", bookRepository.findById(id));
        return "books/edit-book";
    }

    // Edit Part 2
    @PostMapping("books/{id}/edit-book")
    public String updateBook(@PathVariable Integer id, @ModelAttribute Book book){
        bookRepository.save(book);
        return "redirect:/books";
    }


    // Delete all books

    @PostMapping("books/{id}/delete-book")
    public String deleteBook(@PathVariable Integer id){

//        delete a book
        bookRepository.deleteById(id);
        return "redirect:/books";
    }



}
