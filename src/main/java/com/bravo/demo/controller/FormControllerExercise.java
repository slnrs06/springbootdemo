package com.bravo.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller // controller
public class FormControllerExercise {
//    In your FormController
//    Create a controller method to process the HTML form
//    Assign the url “/process-form”


@GetMapping("/show-form")
public String showTheForm(){ // METHOD ATTACHED TO URL

    return "company-form"; // HTML FILE NAME


}
// mapping to process post the information from the  HTML form

    @PostMapping("/process-form") // post mapping is used to post data to the view
    public String processForm(@RequestParam(name = "firstName")String firstName,
                              @RequestParam(name = "lastName") String lastName,
                                @RequestParam(name = "city") String city,
                              @RequestParam(name = "state") String state,
                              @RequestParam(name = "email") String email,
                              Model infoModel){
        //@RequestParam annotation is used to read the form data and bind it automatically to the parameter variable present in the provided method. WE HAVE USED "cohort" to represent example:

        infoModel.addAttribute("name", "Name:  " + firstName + " " + lastName);
        infoModel.addAttribute("hometown", "Hometown:  " + city + " " + state);
        infoModel.addAttribute("contact", "Email:  " + email);
// the first parameter in .addAttribute matches up with the HTML variable that is used in the following syntax in the html example <p th:text="${contact}"></p>
        // the second parameter is the content that will be returned on the html example "Name:"
        return "show-submit-form";
    }

}
