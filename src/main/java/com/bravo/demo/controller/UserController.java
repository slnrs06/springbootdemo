package com.bravo.demo.controller;


import com.bravo.demo.Repositories.UserRepo;
import com.bravo.demo.Repositories.UserRepository;
import com.bravo.demo.models.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;


@Controller
public class UserController {

    // these two variables will provide access Jpa API through repo variable
    // PasswordEncoder variable comes from spring security and will secure the password

    private UserRepo users;
    private PasswordEncoder passwordEncoder;

    public UserController(UserRepo users, PasswordEncoder passwordEncoder) {
        this.users = users;
        this.passwordEncoder = passwordEncoder;
    }

    @GetMapping("/")
    public String landing(){
        return "users/sign-up";
    }
    // sign-up form calling model to send to front end
    @GetMapping("/sign-up")
    public String showSignUpForm(Model model){
        model.addAttribute("user", new User());
        return "users/sign-up";
    }


    // saving user
    @PostMapping("/sign-up")
    public String saveUser(@ModelAttribute User user){
        String hash=passwordEncoder.encode(user.getPassword());  // assigning has to passwordEncoder and attaching .getPassword method onto user to get password.
        boolean debug = passwordEncoder.matches(user.getPassword(), hash);
        user.setPassword(hash);   // setting the password for the user
        users.save(user);  // saving user and user input
        return "redirect:/showNewEmployeeForm";  // simple redirect if credentials are good go to employee form



    }





}

//
//    @Autowired
//    UserRepository userRepo;
//
//    @RequestMapping("/users")
//    public String home(Model model){
//
//        model.addAttribute("users", userRepo.findAll());
//        return "users";
//
//    }

