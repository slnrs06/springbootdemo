package com.bravo.demo.controller;

import com.bravo.demo.Repositories.resaleItemRepo;
import com.bravo.demo.models.resaleItems;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class resaleItemController {

    private resaleItemRepo itemDao;

    public resaleItemController(resaleItemRepo itemDao){
        this.itemDao = itemDao;
    }

    @GetMapping("/resaleItems")
    @ResponseBody
    public List<resaleItems> getAllItems(){
        return itemDao.findAll();
    }

}
