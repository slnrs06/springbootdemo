package com.bravo.demo.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller // this identifies this class to be the communication to the URL path within the spring MVC design pattern
public class HelloSpringController {
//
//    @GetMapping("/hello") // GetMapping annotation to define what URL map this method is identifies with
//
//    @ResponseBody // defines what the response will be once the url is given
//    public String hello(){  // the method called on the response body annotation
//        return "Hello from springBoot!"; // returning string message ...
//    }
//
//
//    @GetMapping("/goodbye")
//    @ResponseBody
//    public String goodbye(){
//        return "Goodbye from SpringBoot!";
//    }
//
//// using RequestMapping with path variable from Spring framework
//// pathing variables - using {number} to pass in variable inside url path
//    @RequestMapping(path = "/favorite/{number}")
//    @ResponseBody
//    public String sayFavNum(@PathVariable int number){ // @PathVariable identifies the variable object to the @RequestMapping url path.
//        return "Looks like your favorite number is:" + number;
//    }
//
//    @RequestMapping(path ="/increment/{number}", method = RequestMethod.GET) // method class inside the Spring Boot framework
//    @ResponseBody // this annotation identifies the method will be the response
//    public String addOne(@PathVariable int number){ // @PathVariable passing in variable to identify to @RequestMapping
//        return number + "plus one is" + (number + 1) + "!";
//
//    } // above is the return statement to return on method logic.


}
