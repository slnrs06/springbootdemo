package com.bravo.demo.controller;
import com.bravo.demo.Repositories.AdRepo;
import com.bravo.demo.Repositories.UserRepository;
import com.bravo.demo.models.Ad;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import java.util.List;
@Controller
//public class AdController {
//
//    @Autowired
//    AdRepo adRepo;
//
//    @RequestMapping("/show")
//    public String home(Model model){
//
//        model.addAttribute("show", adRepo.findAll());
//        return "show";
//
//    }
//}






public class AdController {
    private AdRepo adDao;

    public AdController(AdRepo adDao){
        this.adDao = adDao;
//  the above codes issuing dependency injection-passing objects into the constructor of an object///   when you want to use one class dependent upon another class use dependency injection

    }
    @GetMapping("/ads")
    @ResponseBody
    public List<Ad> getAllAds(){
        return adDao.findAll();
    }
    @GetMapping("/ads/search/{title}")
    public String searchAd(@PathVariable String title, Model model) {
        model.addAttribute("ad", adDao.findByTitle(title));
        return "ads/search";
    }
}
