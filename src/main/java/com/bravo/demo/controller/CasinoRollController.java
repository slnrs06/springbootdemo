package com.bravo.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Random;

@Controller
public class CasinoRollController {
//assign url
@GetMapping("/casino-roll/")

    public String guessRoll(){

    return "guess-roll"; // html page
}

@GetMapping("/casino-roll/{num}")
    public String userGuessNum(@PathVariable int num, Model model) { //@PathVariable-put the url get mapping path.  Model-is a class from the builtin spring.ui within spring framework assigning variable
    int dice = (int)(Math.random()*((5) + 1)); // creating int dice object, assign Math.random
    String message; // String variable called message

    if(dice == num){ // creating if statement to test Math.random algorithm against int number variable
        message = "You guessed correctly";
    }else {
        message = "You guessed wrong";
    }
      model.addAttribute("message", message); // adding to the model class assigning the variable we want to to pass to the HTML as well as our message variable
    return "guess-roll";
}

// second solution

    @GetMapping("/second-casino-roll")
    public String rollingDice(){
    return "first-guess-view";

    }
// PostMapping
    @PostMapping("second-casino-roll")
    public String guessDice(@RequestParam String userGuess,Model model){
    // set up random number generator
    Random random = new Random(); // initializing random class

        // create varible for random number
        int newRandom = random.nextInt(6)+1; // outputting number 1 -6
        model.addAttribute("thisIsTheUserGuess", userGuess);
                model.addAttribute("random", newRandom);

                return "second-guess-view";


    }


}
